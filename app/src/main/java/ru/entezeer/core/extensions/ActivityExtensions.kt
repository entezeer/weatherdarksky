package ru.entezeer.core.extensions

import android.app.Activity
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import ru.entezeer.weatherdarksky.R

/**
 * Created by entezeer on 22.02.19
 */

fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int) {
    supportFragmentManager.beginTransaction()
            .replace(frameId, fragment)
            .commit()
}

fun Activity.slideLeftIn() {
    this.overridePendingTransition(R.anim.right_in, R.anim.right_out)
}

fun Activity.slideRightOut() {
    this.overridePendingTransition(R.anim.left_in, R.anim.left_out)
}
