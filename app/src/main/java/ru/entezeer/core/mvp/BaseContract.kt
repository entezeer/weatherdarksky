package ru.entezeer.core.mvp


/**
 * Created by entezeer on 19.02.19
 */

interface BaseContract {
    interface View<T> {
        fun finishView()

        fun attachPresenter(presenter: T)
    }

    interface Presenter<V> {
        fun attachView(view: V)

        fun detachView()
    }
}