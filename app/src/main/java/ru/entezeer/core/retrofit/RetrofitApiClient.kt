package ru.entezeer.core.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class RetrofitApiClient {
    fun getApiClient(baseUrl: String): Retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
}

