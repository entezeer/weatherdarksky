package ru.entezeer.core.coreUtils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.finishAfterTransition
import android.support.v4.content.ContextCompat
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AlertDialog
import ru.entezeer.weatherdarksky.R

object LocationPermissions {
    const val RECORD_REQUEST_CODE = 101
    fun makeRequest(context: Activity) {
        ActivityCompat.requestPermissions(context,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                RECORD_REQUEST_CODE)
    }

    fun isPermissionGranted(context: Context) =
            (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)


    fun showPermissionRequestRationale(context: Activity) {
        if (!ActivityCompat.shouldShowRequestPermissionRationale(context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            makeRequest(context)
        } else {
            val dialog = AlertDialog.Builder(context, R.style.AlertDialogTheme)
                    .setTitle("Enable permission")
                    .setMessage("Your locations permissions has been denied.\nPlease enable location permission")
                    .setPositiveButton("Get setting") { _, _ ->
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        intent.data = Uri.fromParts("package", "ru.entezeer.weatherdarksky", null)
                        intent.addCategory(Intent.CATEGORY_DEFAULT)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(context, intent, Bundle())
                        finishAfterTransition(context)
                    }
                    .setNegativeButton("Cancel") { _, _ ->
                    }
            dialog.show()
        }
    }

    fun isGpsEnabled(context: Activity): Boolean {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
    }

    fun showAlertForLocationSettings(context: Activity) {
        val dialog = AlertDialog.Builder(context, R.style.AlertDialogTheme)
                .setTitle("Enable Location")
                .setMessage("Your location settings is set to 'Off'.\nPlease enable location and reload application")
                .setPositiveButton("Location setting") { _, _ ->
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivity(context, intent, Bundle())

                }
                .setNegativeButton("Cancel") { _, _ ->

                }
        dialog.show()
    }
}