package ru.entezeer.core.coreUtils

import android.util.Log
import ru.entezeer.weatherdarksky.BuildConfig

/**
 * Created by entezeer 05.02.19
 */

object LogUtil {
    private const val TAG = "LogTAG"
    private val DEBUG = BuildConfig.DEBUG

    fun showLogs(message: String) {
        if (DEBUG) Log.d(TAG, message)
    }
}