package ru.entezeer.weatherdarksky.data.weather.model


class Weather(var timezone: String, var currently: Currently, var daily: Daily)


