package ru.entezeer.weatherdarksky.data.weather.model

class Forecast(var time: Int, var summary: String, var temperatureHigh: Double, var temperatureLow: Double, var icon: String,
               var humidity: Double, var windSpeed: Double, var pressure: Double, var dewPoint: Double)