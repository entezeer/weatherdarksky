package ru.entezeer.weatherdarksky.data.weather.model

class Currently(var time: Int, var summary: String, var temperature: Double, var icon: String)