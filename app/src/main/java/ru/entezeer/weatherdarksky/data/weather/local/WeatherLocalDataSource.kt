package ru.entezeer.weatherdarksky.data.weather.local

import ru.entezeer.weatherdarksky.data.weather.WeatherDataSource
import ru.entezeer.weatherdarksky.data.weather.WeatherDataSource.RequestCallback
import ru.entezeer.weatherdarksky.data.weather.model.Forecast
import ru.entezeer.weatherdarksky.data.weather.model.Weather
import ru.entezeer.core.coreUtils.preferences.SharedStorage

class WeatherLocalDataSource(private val sharedStorage: SharedStorage) : WeatherDataSource {

    //region static

    companion object {
        private var KEY = "WEATHER"
        private var INSTANCE: WeatherLocalDataSource? = null
        fun getInstance(sharedStorage: SharedStorage): WeatherLocalDataSource {
            if (INSTANCE == null) {
                INSTANCE = WeatherLocalDataSource(sharedStorage)
            }
            return INSTANCE!!
        }
    }

    //endregion

    override fun getWeather(lat: Double?, lng: Double?, callback: RequestCallback) {
        if (sharedStorage.contains(KEY)) {
            val weatherData = sharedStorage.getObject(KEY, Weather::class.java)
            if (weatherData != null) {
                callback.onSuccess(weatherData)
            }
        }
    }

    override fun setWeather(result: Weather) {
        sharedStorage.saveObject(KEY, result)
    }

    override fun getDayWeather(day: Int): Forecast? {
        return sharedStorage.getObject(KEY, Weather::class.java)?.daily?.data?.get(day)
    }
}