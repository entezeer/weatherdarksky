package ru.entezeer.weatherdarksky.data.weather

import ru.entezeer.weatherdarksky.data.weather.WeatherDataSource.RequestCallback
import ru.entezeer.weatherdarksky.data.weather.local.WeatherLocalDataSource
import ru.entezeer.weatherdarksky.data.weather.model.Forecast
import ru.entezeer.weatherdarksky.data.weather.model.Weather
import ru.entezeer.weatherdarksky.data.weather.remote.WeatherRemote

class WeatherRepository(
        private val remoteSource: WeatherDataSource,
        private val localSource: WeatherDataSource
) : WeatherDataSource {

    companion object {
        private var INSTANCE: WeatherDataSource? = null
        fun getInstance(remoteSource: WeatherRemote,
                        localSource: WeatherLocalDataSource): WeatherDataSource {
            if (INSTANCE == null) {
                INSTANCE = WeatherRepository(remoteSource, localSource)
            }
            return INSTANCE!!
        }
    }

    private var mCachedWeather: Weather? = null

    override fun getWeather(lat: Double?, lng: Double?, callback: RequestCallback) {
        localSource.getWeather(lat, lng, callback)
        remoteSource.getWeather(lat, lng, object : RequestCallback {
            override fun onSuccess(result: Weather) {
                localSource.setWeather(result)
                mCachedWeather = result
                callback.onSuccess(result)
            }

            override fun onFailure(message: String) {
                callback.onFailure(message)
            }
        })
    }

    override fun setWeather(result: Weather) {
        localSource.setWeather(result)
    }

    override fun getDayWeather(day: Int): Forecast? {
        return mCachedWeather?.daily?.data?.get(day)
    }
}