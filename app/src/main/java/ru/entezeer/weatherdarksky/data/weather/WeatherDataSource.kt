package ru.entezeer.weatherdarksky.data.weather

import ru.entezeer.core.callback.BaseCallback
import ru.entezeer.weatherdarksky.data.weather.model.Forecast
import ru.entezeer.weatherdarksky.data.weather.model.Weather

interface WeatherDataSource {
    fun getWeather(lat: Double?, lng: Double?, callback: RequestCallback)
    fun setWeather(result: Weather)
    fun getDayWeather(day: Int): Forecast?

    interface RequestCallback : BaseCallback<Weather>
}
