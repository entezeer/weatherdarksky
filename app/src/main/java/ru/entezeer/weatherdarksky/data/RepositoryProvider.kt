package ru.entezeer.weatherdarksky.data

import ru.entezeer.weatherdarksky.data.weather.WeatherDataSource
import ru.entezeer.weatherdarksky.data.weather.local.WeatherLocalDataSource
import ru.entezeer.weatherdarksky.data.weather.WeatherRepository
import ru.entezeer.weatherdarksky.data.weather.remote.WeatherRemote
import ru.entezeer.core.coreUtils.preferences.SharedStorage

object RepositoryProvider {
    fun getWeatherDataSource(sharedStorage: SharedStorage): WeatherDataSource = WeatherRepository.getInstance(
            WeatherRemote.getInstance(),
            WeatherLocalDataSource.getInstance(sharedStorage)
    )
}