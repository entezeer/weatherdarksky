package ru.entezeer.weatherdarksky.data.weather.remote

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.entezeer.core.retrofit.RetrofitApiClient
import ru.entezeer.weatherdarksky.data.weather.WeatherDataSource
import ru.entezeer.weatherdarksky.data.weather.WeatherDataSource.RequestCallback
import ru.entezeer.weatherdarksky.data.weather.model.Forecast
import ru.entezeer.weatherdarksky.data.weather.model.Weather
import ru.entezeer.core.coreUtils.LogUtil

class WeatherRemote : RetrofitApiClient(),
        WeatherDataSource {

    companion object {
        private var INSTANCE: WeatherRemote? = null
        fun getInstance(): WeatherRemote {
            if (INSTANCE == null) {
                INSTANCE = WeatherRemote()
            }
            return INSTANCE!!
        }
    }

    private var apiWeather: WeatherNetworkClient = getApiClient(WeatherRemoteConstants.BASE_URL).create(WeatherNetworkClient::class.java)

    override fun getWeather(
            lat: Double?,
            lng: Double?,
            callback: RequestCallback
    ) {
        apiWeather.getData(WeatherRemoteConstants.KEY, lat, lng).enqueue(object : Callback<Weather> {
            override fun onFailure(call: Call<Weather>?, t: Throwable) {
                LogUtil.showLogs("Failure")
                callback.onFailure(t.message.toString())
            }

            override fun onResponse(call: Call<Weather>?, response: Response<Weather>) {
                if (response.isSuccessful) {
                    if (response.body() != null) {
                        response.body()?.let { callback.onSuccess(it) }
                    }
                    else callback.onFailure("body is null")
                } else {
                    LogUtil.showLogs("Response is not successful")
                    callback.onFailure("Error. response is not successful")
                }
            }

        })
    }

    override fun setWeather(result: Weather) {
    }

    override fun getDayWeather(day: Int): Forecast? {
        return null
    }
}