package ru.entezeer.weatherdarksky.data.weather.remote

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import ru.entezeer.weatherdarksky.data.weather.model.Weather

interface WeatherNetworkClient {
    @GET("{key}/{lat},{lng}")
    fun getData(
            @Path("key") key: String,
            @Path("lat") lat: Double?,
            @Path("lng") lng: Double?
    ): Call<Weather>
}