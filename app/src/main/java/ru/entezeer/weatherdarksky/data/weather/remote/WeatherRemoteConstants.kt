package ru.entezeer.weatherdarksky.data.weather.remote

object WeatherRemoteConstants {
    const val BASE_URL = "https://api.darksky.net/forecast/"
    const val KEY = "c0e4da6da0306384ea0eb1188035ee17"
}