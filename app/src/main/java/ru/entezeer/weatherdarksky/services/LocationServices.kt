@file:Suppress("DEPRECATION")

package ru.entezeer.weatherdarksky.services

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import ru.entezeer.core.coreUtils.BroadcastUtils
import ru.entezeer.core.coreUtils.BroadcastUtils.EXTRA_LOCATION_OFF
import ru.entezeer.core.coreUtils.LocationPermissions

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class LocationServices : Service(),
        com.google.android.gms.location.LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    var googleApiClient: GoogleApiClient? = null
    private var location: Location? = null
    private var locationRequest: LocationRequest? = null
    private val UPDATE_INTERVAL: Long = 2000
    private val FASTEST_INTERVAL: Long = (UPDATE_INTERVAL / 2)
    private val locationBinder = LocalBinder()
    private var locationManager: LocationManager? = null

    override fun onCreate() {
        super.onCreate()
        buildGoogleApiClient()
        locationManager = this.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (googleApiClient == null) {
            googleApiClient!!.connect()
        }
    }


    override fun onBind(intent: Intent?): IBinder {
        return locationBinder
    }

    inner class LocalBinder : Binder() {
        fun getService(): ru.entezeer.weatherdarksky.services.LocationServices {
            return this@LocationServices
        }
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        if (!LocationPermissions.isPermissionGranted(this)) {
            return
        }

        location = locationManager?.getLastKnownLocation(locationManager?.getBestProvider(Criteria(), false))

        if (location == null) {
            BroadcastUtils.sendMessage(this, EXTRA_LOCATION_OFF)
            startLocationUpdates()
        }

        if (location != null) {
            BroadcastUtils.sendLocation(this, location?.latitude!!, location?.longitude!!)
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        googleApiClient!!.connect()
        startLocationUpdates()
    }


    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            BroadcastUtils.sendLocation(this@LocationServices, location.latitude, location.longitude)
            locationManager?.removeUpdates(this)
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    override fun onLocationChanged(location: Location) {
        BroadcastUtils.sendLocation(this, location.latitude, location.longitude)
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this)
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates() {
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL)
        if (!LocationPermissions.isPermissionGranted(this)) {
            return
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this)
        locationManager?.requestLocationUpdates(locationManager?.getBestProvider(Criteria(), false), 0L, 800f, locationListener)
    }

    @Synchronized
    private fun buildGoogleApiClient() {
        googleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build()
        googleApiClient!!.connect()
    }
}