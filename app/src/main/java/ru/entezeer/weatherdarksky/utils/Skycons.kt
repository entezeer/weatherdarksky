package ru.entezeer.weatherdarksky.utils

import android.widget.ImageView
import ru.entezeer.weatherdarksky.R

fun ImageView.setWeatherIcon(type: String?) {
    setImageResource(when(type){
        "sleet"->R.drawable.ic_weather_snowy_white_48dp
        "clear" -> R.drawable.ic_weather_sunny_white_48dp
        "cloudy" -> R.drawable.ic_weather_cloudy_white_48dp
        "partly-cloudy" -> R.drawable.ic_weather_partlycloudy_white_48dp
        "partly-cloudy-day" -> R.drawable.ic_weather_partlycloudy_white_48dp
        "partly-cloudy-night" -> R.drawable.ic_weather_partlycloudy_white_48dp
        "thunder" -> R.drawable.ic_weather_lightning_white_48dp
        "clear-night" -> R.drawable.ic_weather_night_white_48dp
        "snow" -> R.drawable.ic_weather_snowy_white_48dp
        "heavy-showers" -> R.drawable.ic_weather_pouring_white_48dp
        "rain" -> R.drawable.ic_weather_rainy_white_48dp
        "fog" -> R.drawable.ic_weather_fog_white_48dp
        else -> R.drawable.ic_weather_sunny_white_48dp
    })
}
