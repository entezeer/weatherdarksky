package ru.entezeer.weatherdarksky.utils

import java.text.SimpleDateFormat
import java.util.*

fun Double.convertPressure() = "${(this * 0.75).toInt()} mmHg"

fun Double.convertHumidity() = "${(this * 100).toInt()} %"

fun Double.convertTemp() = "${((this - 32) * 5 / 9).toInt()} \u00B0 C"

fun Double.convertSpeed() = "${(this * 1.6).toInt()} km/H"

fun Int.convertUNIX(): String = SimpleDateFormat(
        "EEEE, dd MMM.",
        Locale.ENGLISH
).format(Date(this.toLong() * 1000))

