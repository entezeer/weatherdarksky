package ru.entezeer.weatherdarksky.ui.details

import ru.entezeer.weatherdarksky.data.weather.WeatherDataSource

/**
 * Created by entezeer on 20.02.19
 */

class DetailWeatherPresenter(
        private val mWeatherDataSource: WeatherDataSource
) : DetailWeatherContract.Presenter {

    private var mView: DetailWeatherContract.View? = null

    override fun attachView(view: DetailWeatherContract.View) {
        mView = view
        view.attachPresenter(this)
    }

    override fun detachView() {
        mView = null
    }

    override fun getData(id: Int) {
        val dayWeather = mWeatherDataSource.getDayWeather(id)

        if (dayWeather == null) {
            mView?.showLoadingError()
        } else {
            mView?.showWeather(dayWeather)
        }
    }
}