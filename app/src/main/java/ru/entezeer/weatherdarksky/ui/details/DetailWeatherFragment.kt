package ru.entezeer.weatherdarksky.ui.details

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import ru.entezeer.core.extensions.slideRightOut
import ru.entezeer.weatherdarksky.R
import ru.entezeer.weatherdarksky.data.weather.model.Forecast
import ru.entezeer.weatherdarksky.utils.*

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DetailWeatherFragment : Fragment(), DetailWeatherContract.View {

    var rootView: View? = null
    private var detailSummary: TextView? = null
    private var temperatureHigh: TextView? = null
    private var temperatureLow: TextView? = null
    private var humidity: TextView? = null
    private var pressure: TextView? = null
    private var wind: TextView? = null
    private var dewPoint: TextView? = null
    private var day: TextView? = null
    private var detailWeatherIcon: ImageView? = null
    private var backButton: FloatingActionButton? = null

    private var mPresenter: DetailWeatherContract.Presenter? = null

    //region Lifecycle

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail_weather, container, false)

        rootView = view
        init(view)
        return view
    }

    //endregion

    @SuppressLint("StringFormatMatches", "SetTextI18n")
    fun init(rootView: View) {
        val id = arguments?.getInt(ARG_ID)
        detailSummary = rootView.findViewById(R.id.detail_summary)
        temperatureHigh = rootView.findViewById(R.id.temperature_high)
        temperatureLow = rootView.findViewById(R.id.temperature_low)
        day = rootView.findViewById(R.id.day)
        detailWeatherIcon = rootView.findViewById(R.id.detail_weather_icon)
        humidity = rootView.findViewById(R.id.humidity)
        pressure = rootView.findViewById(R.id.pressure)
        wind = rootView.findViewById(R.id.wind)
        dewPoint = rootView.findViewById(R.id.dew_point)
        backButton = rootView.findViewById(R.id.back_fab)


        backButton?.setOnClickListener {
            activity?.onBackPressed()
        }
        id?.let { mPresenter?.getData(it) }
    }

    //region MVP

    @SuppressLint("SetTextI18n", "NewApi")
    override fun showWeather(weatherData: Forecast) {
        detailWeatherIcon?.setWeatherIcon(weatherData.icon)
        detailSummary?.text = weatherData.summary
        temperatureHigh?.text = getString(R.string.day) + weatherData.temperatureHigh.convertTemp()
        temperatureLow?.text = getString(R.string.night) + weatherData.temperatureLow.convertTemp()
        day?.transitionName = arguments?.getString(ARG_TRANSITION_NAME)
        day?.text = arguments?.getString(ARG_ITEM)
        pressure?.text = weatherData.pressure.convertPressure()
        humidity?.text = weatherData.humidity.convertHumidity()
        dewPoint?.text = weatherData.dewPoint.convertTemp()
        wind?.text = weatherData.windSpeed.convertSpeed()
    }

    override fun showLoadingError() {
        temperatureHigh?.text = getString(R.string.noData)
    }

    override fun finishView() {
        activity?.finish()
        activity?.slideRightOut()
    }

    override fun attachPresenter(presenter: DetailWeatherContract.Presenter) {
        mPresenter = presenter
    }

    //endregion

    companion object {
        private const val ARG_ID = "id"
        private const val ARG_ITEM = "item"
        private const val ARG_TRANSITION_NAME = "transition"
        fun newInstance(id: Int, title: String, transitionName: String): DetailWeatherFragment = DetailWeatherFragment().apply {
            val bundle = Bundle()
            bundle.putInt(ARG_ID, id)
            bundle.putString(ARG_ITEM, title)
            bundle.putString(ARG_TRANSITION_NAME, transitionName)
            this.arguments = bundle
        }
    }
}
