package ru.entezeer.weatherdarksky.ui.details

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.content.ContextCompat.startActivity
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import ru.entezeer.core.extensions.addFragment
import ru.entezeer.weatherdarksky.R
import ru.entezeer.weatherdarksky.WeatherApplication
import ru.entezeer.weatherdarksky.data.RepositoryProvider


class DetailWeatherActivity : AppCompatActivity() {

    private var mPresenter: DetailWeatherContract.Presenter? = null

    //region Lifecycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_weather)
        val id = intent.getIntExtra(EXTRA_ID, 0)
        val title = intent.getStringExtra(EXTRA_ITEM)
        val transitionName = intent.getStringExtra(EXTRA_TRANSITION_NAME)
        val detailWeatherFragment = DetailWeatherFragment.newInstance(id, title, transitionName)
        mPresenter = DetailWeatherPresenter(RepositoryProvider.getWeatherDataSource(WeatherApplication.sharedStorage))
        mPresenter?.attachView(detailWeatherFragment)
        addFragment(detailWeatherFragment, R.id.detail_weather_fragment)
    }

    //endregion

    companion object {
        private const val EXTRA_ID = "id"
        private const val EXTRA_ITEM = "item"
        private const val EXTRA_TRANSITION_NAME = "transition"

        fun start(context: Activity, id: Int, title: String, textView: TextView) {
            val intent = Intent(context, DetailWeatherActivity::class.java)
            intent.putExtra(EXTRA_ID, id)
            intent.putExtra(EXTRA_ITEM, title)
            intent.putExtra(EXTRA_TRANSITION_NAME, ViewCompat.getTransitionName(textView))

            val options = ViewCompat.getTransitionName(textView)?.let {
                ActivityOptionsCompat.makeSceneTransitionAnimation(
                        context,
                        textView,
                        it)
            }
            startActivity(context, intent, options?.toBundle())
        }
    }
}
