package ru.entezeer.weatherdarksky.ui.main

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import ru.entezeer.weatherdarksky.R
import ru.entezeer.weatherdarksky.WeatherApplication
import ru.entezeer.weatherdarksky.data.RepositoryProvider
import ru.entezeer.core.extensions.addFragment
import ru.entezeer.weatherdarksky.ui.weather.WeatherContract
import ru.entezeer.weatherdarksky.ui.weather.WeatherFragment
import ru.entezeer.weatherdarksky.ui.weather.WeatherPresenter
import ru.entezeer.core.coreUtils.LocationPermissions
import ru.entezeer.core.coreUtils.LocationPermissions.isGpsEnabled

class MainActivity : AppCompatActivity() {

    private var mPresenter: WeatherContract.Presenter? = null

    //region lifeCycle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        checkPermissions()
        val weatherFragment = WeatherFragment.newInstance()
        mPresenter = WeatherPresenter(RepositoryProvider.getWeatherDataSource(WeatherApplication.sharedStorage))
        mPresenter?.attachView(weatherFragment)
        addFragment(weatherFragment, R.id.weather_fragment)
    }

    //endregion

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            LocationPermissions.RECORD_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    init()
                } else {
                    Toast.makeText(this, "Permission has been denied by user", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun init() {
        if (!isGpsEnabled(this)) {
            LocationPermissions.showAlertForLocationSettings(this)
        }
    }

    private fun checkPermissions() {
        if (LocationPermissions.isPermissionGranted(this)) {
            LocationPermissions.makeRequest(this)
        } else LocationPermissions.showPermissionRequestRationale(this)
    }

    companion object {
        fun start(context: Activity) {
            startActivity(context, Intent(context, MainActivity::class.java), Bundle())
        }
    }

}