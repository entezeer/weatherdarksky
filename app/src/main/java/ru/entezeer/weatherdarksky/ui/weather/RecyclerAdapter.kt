package ru.entezeer.weatherdarksky.ui.weather

import android.annotation.SuppressLint
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import ru.entezeer.weatherdarksky.R
import ru.entezeer.weatherdarksky.data.weather.model.Forecast
import ru.entezeer.weatherdarksky.utils.convertTemp
import ru.entezeer.weatherdarksky.utils.convertUNIX
import ru.entezeer.weatherdarksky.utils.setWeatherIcon
import java.util.*

class RecyclerAdapter(private val items: ArrayList<Forecast>, var listener: Listener) :
        RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycler_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bindData(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val recyclerDay = itemView.findViewById<TextView>(R.id.recyclerDay)
        private val recyclerTemperatureHigh = itemView.findViewById<TextView>(R.id.recycler_temperature_high)
        private val recyclerTemperatureLow = itemView.findViewById<TextView>(R.id.recycler_temperature_low)
        private val recyclerImage = itemView.findViewById<ImageView>(R.id.recycler_image)
        private val recyclerSummary = itemView.findViewById<TextView>(R.id.recycler_summary)

        init {
            itemView.setOnClickListener {
                val title = items[adapterPosition].time.convertUNIX()
                ViewCompat.setTransitionName(recyclerDay, title)
                listener.onItemSelectedAt(adapterPosition, title, recyclerDay)
            }
        }

        fun bindData(items: Forecast) {
            recyclerDay.text = items.time.convertUNIX()
            recyclerTemperatureHigh.text = items.temperatureHigh.convertTemp()
            recyclerTemperatureLow.text = items.temperatureLow.convertTemp()
            recyclerSummary.text = items.summary
            recyclerImage.setWeatherIcon(items.icon)
        }
    }

    interface Listener {
        fun onItemSelectedAt(position: Int, title: String, textView: TextView)
    }
}