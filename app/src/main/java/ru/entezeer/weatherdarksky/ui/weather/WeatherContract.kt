package ru.entezeer.weatherdarksky.ui.weather

import android.widget.TextView
import ru.entezeer.core.mvp.BaseContract
import ru.entezeer.weatherdarksky.data.weather.model.Weather
import ru.entezeer.weatherdarksky.services.LocationServices

/**
 * Created by entezeer on 19.02.19
 */

interface WeatherContract {
    interface View : BaseContract.View<Presenter> {
        fun showLoading()

        fun hideLoading()

        fun showWeather(weather: Weather)

        fun openForecastDetails(id: Int, title: String, textView: TextView)

        fun showMessage(message: String)

        fun showRetryView()

        fun hideRetryView()

        fun showOnFailureAlert()

        fun refresh()
    }

    interface Presenter : BaseContract.Presenter<View> {

        fun restoreDataFromShared()

        fun onRefresh()

        fun onLocationChanged(lat: Double, lng: Double)

        fun onForecastDayClick(position: Int, title: String, textView: TextView)

        fun startLocationUpdates(services: LocationServices)
    }
}