package ru.entezeer.weatherdarksky.ui.weather

import android.widget.TextView
import ru.entezeer.weatherdarksky.data.weather.WeatherDataSource
import ru.entezeer.weatherdarksky.data.weather.model.Weather
import ru.entezeer.weatherdarksky.services.LocationServices

/**
 * Created by entezeer on 19.02.19
 */

class WeatherPresenter(
        private val mWeatherDataSource: WeatherDataSource
) : WeatherContract.Presenter {
    private var mView: WeatherContract.View? = null
    private var mCachedData: Weather? = null

    override fun restoreDataFromShared() {
        mView?.hideRetryView()
        mView?.showLoading()
        mWeatherDataSource.getWeather(null, null, object : WeatherDataSource.RequestCallback {
            override fun onSuccess(result: Weather) {
                mView?.showWeather(result)
                mCachedData = result
            }

            override fun onFailure(message: String) {
                if (mCachedData == null) {
                    mView?.hideLoading()
                    mView?.showRetryView()
                }
            }
        })
    }

    override fun onForecastDayClick(position: Int, title: String, textView: TextView) {
        if (mCachedData != null) {
            mView?.openForecastDetails(position, title, textView)
        } else mView?.showMessage("No data")
    }

    override fun attachView(view: WeatherContract.View) {
        mView = view
        view.attachPresenter(this)
    }

    override fun detachView() {
        mView = null
    }

    override fun onLocationChanged(lat: Double, lng: Double) {
        mView?.showLoading()
        mWeatherDataSource.getWeather(lat, lng, object : WeatherDataSource.RequestCallback {
            override fun onSuccess(result: Weather) {
                mView?.showWeather(result)
                mCachedData = result
            }

            override fun onFailure(message: String) {
                mView?.showOnFailureAlert()
            }
        })
    }

    override fun startLocationUpdates(services: LocationServices) {
        services.startLocationUpdates()
    }

    override fun onRefresh() {
        mView?.refresh()
    }
}