package ru.entezeer.weatherdarksky.ui.details

import ru.entezeer.weatherdarksky.data.weather.model.Forecast
import ru.entezeer.core.mvp.BaseContract

/**
 * Created by entezeer on 19.02.19
 */

interface DetailWeatherContract {
    interface View : BaseContract.View<Presenter> {
        fun showWeather(weatherData: Forecast)

        fun showLoadingError()
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun getData(id: Int)
    }
}