package ru.entezeer.weatherdarksky.ui.weather

import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.CardView
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import ru.entezeer.core.coreUtils.BroadcastUtils
import ru.entezeer.core.coreUtils.BroadcastUtils.BROADCAST_ACTION
import ru.entezeer.core.coreUtils.BroadcastUtils.EXTRA_LAT
import ru.entezeer.core.coreUtils.BroadcastUtils.EXTRA_LNG
import ru.entezeer.core.coreUtils.BroadcastUtils.EXTRA_LOCATION_OFF
import ru.entezeer.core.coreUtils.BroadcastUtils.EXTRA_MESSAGE
import ru.entezeer.core.coreUtils.LocationPermissions
import ru.entezeer.core.extensions.slideRightOut
import ru.entezeer.weatherdarksky.R
import ru.entezeer.weatherdarksky.data.weather.model.Weather
import ru.entezeer.weatherdarksky.services.LocationServices
import ru.entezeer.weatherdarksky.ui.details.DetailWeatherActivity
import ru.entezeer.weatherdarksky.utils.convertTemp
import ru.entezeer.weatherdarksky.utils.setWeatherIcon

@Suppress("DEPRECATION")
class WeatherFragment : Fragment(),
        RecyclerAdapter.Listener, WeatherContract.View {


    private var mPresenter: WeatherContract.Presenter? = null
    private var weatherRecyclerView: RecyclerView? = null
    private var swipeRefreshLayout: SwipeRefreshLayout? = null
    private var retryButton: Button? = null
    private var retryView: CardView? = null
    var myService: LocationServices? = null
    private var locationName: TextView? = null
    private var temperature: TextView? = null
    private var weatherIcon: ImageView? = null
    private var summary: TextView? = null
    private var today: TextView? = null
    var isBound = false


    private var locationReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action.equals(BROADCAST_ACTION)) {
                val message = intent?.getStringExtra(EXTRA_MESSAGE)
                if (message == EXTRA_LOCATION_OFF) {
                    mPresenter?.restoreDataFromShared()
                } else {
                    mPresenter?.onLocationChanged(intent?.getDoubleExtra(EXTRA_LAT, 0.0)!!, intent.getDoubleExtra(EXTRA_LNG, 0.0))
                }
            }
        }
    }

    private val myConnection = object : ServiceConnection {
        override fun onServiceDisconnected(p0: ComponentName?) {
            isBound = false
        }

        override fun onServiceConnected(p0: ComponentName?, service: IBinder?) {
            val binder = service as LocationServices.LocalBinder
            myService = binder.getService()
            isBound = true
        }
    }

    //region Lifecycle

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_weather, container, false)
        // Inflate the layout for this fragment
        init(view)
        mPresenter?.restoreDataFromShared()
        activity?.let {
            BroadcastUtils.register(locationReceiver, it)
            it.bindService(Intent(context, LocationServices::class.java), myConnection, Context.BIND_AUTO_CREATE)
        }
        return view
    }

    override fun onDestroy() {
        activity?.let { BroadcastUtils.unRegister(locationReceiver, it) }
        mPresenter?.detachView()
        super.onDestroy()
    }

    override fun onStop() {
        super.onStop()
        if (isBound) {
            activity!!.unbindService(myConnection)
            isBound = false
        }
    }

    //endregion

    override fun onItemSelectedAt(position: Int, title: String, textView: TextView) {
        mPresenter?.onForecastDayClick(position, title, textView)
    }

    //initRecyclerView

    private fun init(rootView: View?) {
        retryView = rootView?.findViewById(R.id.retry_view)
        retryButton = rootView?.findViewById<Button>(R.id.retry_button)
        locationName = rootView?.findViewById(R.id.location_name)
        temperature = rootView?.findViewById(R.id.temperature)
        weatherIcon = rootView?.findViewById(R.id.weather_icon)
        summary = rootView?.findViewById(R.id.summary)
        today = rootView?.findViewById(R.id.today)

        swipeRefreshLayout = rootView?.findViewById(R.id.swipeRefreshLayout) as SwipeRefreshLayout

        weatherRecyclerView = rootView.findViewById(R.id.weather_recycler) as RecyclerView
        weatherRecyclerView?.layoutManager = LinearLayoutManager(activity)

        val dividerItemDecoration = DividerItemDecoration(activity, DividerItemDecoration.VERTICAL)
        dividerItemDecoration.setDrawable(activity?.resources?.getDrawable(R.drawable.divide_line)!!)
        weatherRecyclerView?.addItemDecoration(dividerItemDecoration)

        swipeRefreshLayout?.setOnRefreshListener {
            mPresenter?.onRefresh()
        }
        retryButton?.setOnClickListener {
            clickRetry()
        }

    }

    private fun clickRetry() {
        mPresenter?.restoreDataFromShared()
        activity?.let {
            if (LocationPermissions.isPermissionGranted(it)) {
                if (LocationPermissions.isGpsEnabled(it)) {
                    myService?.startLocationUpdates()
                } else {
                    LocationPermissions.showAlertForLocationSettings(it)
                    hideLoading()
                }
            } else {
                LocationPermissions.showPermissionRequestRationale(it)
            }
        }
        retryView?.visibility = View.GONE
    }

    //region MVP

    override fun showLoading() {
        swipeRefreshLayout?.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout?.isRefreshing = false
    }

    override fun showWeather(weather: Weather) {
        weatherRecyclerView?.adapter = RecyclerAdapter(weather.daily.data, this)
        weatherRecyclerView?.setOnClickListener {
            startActivity(Intent(activity, DetailWeatherActivity::class.java))
        }
        locationName?.text = weather.timezone
        temperature?.text = weather.currently.temperature.convertTemp()
        weatherIcon?.setWeatherIcon(weather.currently.icon)
        summary?.text = weather.currently.summary
        today?.text = getString(R.string.today)
        swipeRefreshLayout?.isRefreshing = false
        hideRetryView()
    }


    override fun showRetryView() {
        retryView?.visibility = View.VISIBLE
    }

    override fun refresh() {
        activity?.let {
            if (LocationPermissions.isPermissionGranted(it)) {
                if (LocationPermissions.isGpsEnabled(it)) {
                    showLoading()
                    myService?.startLocationUpdates()
                } else {
                    LocationPermissions.showAlertForLocationSettings(it)
                    hideLoading()
                }
            } else LocationPermissions.showPermissionRequestRationale(it)
        }
    }

    override fun hideRetryView() {
        retryView?.visibility = View.GONE
    }

    override fun openForecastDetails(id: Int, title: String, textView: TextView) {
        activity?.let { DetailWeatherActivity.start(it, id, title, textView) }
    }

    override fun showMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    override fun finishView() {
        activity?.finish()
        activity?.slideRightOut()
    }

    override fun attachPresenter(presenter: WeatherContract.Presenter) {
        mPresenter = presenter
    }

    override fun showOnFailureAlert() {
        val dialog = AlertDialog.Builder(activity!!, R.style.AlertDialogTheme)
        dialog.setTitle(R.string.connectionError)?.setMessage(R.string.checkConnection)
                ?.setPositiveButton(R.string.tryAgain) { _, _ ->
                    mPresenter?.startLocationUpdates(myService!!)
                }
                ?.setNegativeButton(R.string.cancel) { _, _ -> }
                ?.show()
    }

    //endregion

    companion object {
        fun newInstance(): WeatherFragment = WeatherFragment()
    }
}
