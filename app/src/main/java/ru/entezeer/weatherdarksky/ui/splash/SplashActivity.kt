package ru.entezeer.weatherdarksky.ui.splash

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.entezeer.weatherdarksky.R
import ru.entezeer.weatherdarksky.ui.main.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        MainActivity.start(this)
        finish()
    }
}
