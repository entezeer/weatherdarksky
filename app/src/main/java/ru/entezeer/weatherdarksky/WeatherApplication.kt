package ru.entezeer.weatherdarksky

import android.app.Application
import ru.entezeer.core.coreUtils.preferences.SharedStorage
import ru.entezeer.core.coreUtils.preferences.SharedStorageImpl

open class WeatherApplication : Application() {

    companion object {
        lateinit var sharedStorage: SharedStorage
        private const val SHARED_NAME = "PREF"
    }

    override fun onCreate() {
        super.onCreate()
        sharedStorage = SharedStorageImpl(this, SHARED_NAME)
    }
}